//
//  FirstViewController.swift
//  ReusablePopups
//
//  Created by rafiul hasan on 21/2/22.
//

import UIKit

class FirstViewController: UIViewController {
    
    @IBOutlet weak var dateLabel: UILabel!    
    var observer: NSObjectProtocol?
    
    let checkbox = CheckboxButton(frame: CGRect(x: 70, y: 200, width: 40, height: 40))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let label = UILabel(frame: CGRect(x: 115, y: 200, width: 200, height: 40))
        label.text = "Term and conditions"
        view.addSubview(label)
        view.addSubview(checkbox)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapCheckbox))
        checkbox.addGestureRecognizer(gesture)
    }
    
    @objc func didTapCheckbox() {
        checkbox.toggle()
    }
    
    // start notificationcenter way
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //1. Notification old way
        //NotificationCenter.default.addObserver(self, selector:#selector(handlePopupClosing), name: .saveDateTime, object: nil)
        //2. Notification new way
        observer = NotificationCenter.default.addObserver(forName: .saveDateTime, object: nil, queue: .main) { notification in
            let dateVC = notification.object as! DatePickerVC
            self.dateLabel.text = dateVC.formattedDate
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let observer = observer {
            NotificationCenter.default.removeObserver(observer)
        }
    }
    @objc func handlePopupClosing(notification: Notification) {
        let dateVC = notification.object as! DatePickerVC
        dateLabel.text = dateVC.formattedDate
    }
    // end notificationcenter way
}
