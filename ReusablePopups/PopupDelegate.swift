//
//  PopupDelegate.swift
//  ReusablePopups
//
//  Created by rafiul hasan on 21/2/22.
//

import Foundation

protocol PopupDelegate: AnyObject {
    func popupValueSelected(value: String)
}
