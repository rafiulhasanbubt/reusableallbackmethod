//
//  DatePickerVC.swift
//  ReusablePopups
//
//  Created by rafiul hasan on 21/2/22.
//

import UIKit

class DatePickerVC: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var saveButton: UIButton!
    
    var showTimePicker: Bool = false
    var onSave: ((_ data: String) -> ())?    
    var delegate: PopupDelegate?
    
    var formattedDate: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: datePicker.date)
    }
    var formattedTime: String {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        return formatter.string(from: datePicker.date)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if showTimePicker {
            titleLabel.text = "Select Time"
            datePicker.datePickerMode = .time
            saveButton.setTitle("Save Time", for: .normal)
        } else {
            titleLabel.text = "Select Date"
            datePicker.minimumDate = Date()
            datePicker.datePickerMode = .date
            saveButton.setTitle("Save Date", for: .normal)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func donePressed(_ sender: UIButton) {
        //MARK: Method for Notifications
        NotificationCenter.default.post(name: .saveDateTime, object: self)
        
        //MARK: Method for Callback
        if showTimePicker {
            //MARK: Method for Callback
            onSave?(formattedTime)
            //MARK: Method for delegate
            //delegate?.popupValueSelected(value: formattedTime)
        } else {
            //MARK: Method for Callback
            onSave?(formattedDate)
            //MARK: Method for delegate
            //delegate?.popupValueSelected(value: formattedDate)
        }
        //MARK: Method for delegate
        if showTimePicker {
            delegate?.popupValueSelected(value: formattedTime)
        } else {
            delegate?.popupValueSelected(value: formattedDate)
        }
        dismiss(animated: true, completion: nil)
    }
}
