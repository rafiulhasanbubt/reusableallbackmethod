//
//  ThirdViewController.swift
//  ReusablePopups
//
//  Created by rafiul hasan on 21/2/22.
//

import UIKit

class ThirdViewController: UIViewController {

    @IBOutlet weak var dateLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func getTime(_ sender: UIButton) {
        let sb = UIStoryboard(name: "DatePickerVC", bundle: nil)
        let popup = sb.instantiateInitialViewController()! as! DatePickerVC
        popup.showTimePicker = true
        popup.delegate = self
        self.present(popup, animated: true, completion: nil)
    }    
}

extension ThirdViewController: PopupDelegate {
    func popupValueSelected(value: String) {
        dateLabel.text = value
    }
}
