//
//  NotificationNameExtension.swift
//  ReusablePopups
//
//  Created by rafiul hasan on 21/2/22.
//

import Foundation

extension Notification.Name {
    static let saveDateTime = NSNotification.Name(rawValue: "saveDateTime")
}
