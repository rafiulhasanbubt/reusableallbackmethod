//
//  CheckboxButton.swift
//  ReusablePopups
//
//  Created by rafiul hasan on 22/2/22.
//

import UIKit

class CheckboxButton: UIView {

    var isChecked = false
    
    let imageView: UIImageView  = {
        let imageView = UIImageView()
        imageView.isHidden = false
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .systemBlue
        imageView.image = UIImage(systemName: "checkmark")
        return imageView
    }()
    
    let boxView: UIView = {
        let view = UIView()
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.label.cgColor
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .systemRed
        addSubview(imageView)
    }
    required init?(coder: NSCoder) {
        fatalError()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    public func toggle() {
        self.isChecked = !isChecked
        imageView.isHidden = isChecked
    }
}
