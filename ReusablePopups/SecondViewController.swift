//
//  SecondViewController.swift
//  ReusablePopups
//
//  Created by rafiul hasan on 21/2/22.
//

import UIKit

class SecondViewController: UIViewController {
    @IBOutlet weak var dateLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //start Method for Callback
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "presenter" {
            let popup = segue.destination as! DatePickerVC
            //1. assign to a function
            //popup.onSave = onSave
            
            //2. use a closure
            popup.onSave = { data in
                self.dateLabel.text = data
            }
        }
    }
    func onSave(_ data: String) -> () {
        dateLabel.text = data
    }
    //end Method for Callback
}
